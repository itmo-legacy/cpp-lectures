#include <iostream>

struct Base {
    virtual void foo() const {
        std::cout << "Base" << std::endl;
    }
};

struct Derived : Base {
    void foo() const override {
        std::cout << "Derived" << std::endl;
    }
};

int main() {
    try {
        try {
            throw Derived();
        } catch (Base const &e) {
            e.foo();
            throw;
        }
    } catch (Base const &e) {
        e.foo();
    }
}
