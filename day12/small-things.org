* =decltype=
** rationale
   Sometimes, it is unclear what should be a return type of a function:
   #+BEGIN_SRC C++
     vector<int> g(int, float);
     char g(string &, int);
     template <typename T> T *g(void);

     template <typename... T>
     ?type-name f(T&& ...args) { // what should be instead of ?type-name
         return g(std::forward<T>(args)...);
     }
   #+END_SRC

   Another example:
   #+BEGIN_SRC C++
     template <typename U, typename V>
     typename std::common_type_t<U, V> max(U const &u, V const& v) {
         return u < v ? v : u;
     }
   #+END_SRC
   Here, =common_type_t= represents the type that can present values from both
   =U= and =V=. However, it doesn't work for user-defined types.
  
   A more general solution is to use =decltype=. =decltype(expression)=
   substitutes resulting type of expression without executing it.
   #+BEGIN_SRC C++
     template <typename ...T>
     decltype(g(std::forward<T>(args)...)) // `args' before their declaration
     f(T &&...args) {
         return g(std::forward<T>(args)...);
     }
   #+END_SRC

   However, it doesn't work because =decltype= uses =args= before their declration.
   To fix this, one might use ~trailing-type-declaration~.
   #+BEGIN_SRC C++
     template <typename ...T>
     auto f(T &&...args) -> decltype(g(std::forward<T>(args)...)) {…}
   #+END_SRC

   N.B. Code inside of =decltype= and =sizeof= is called ~non-evaluating-context~.

   How is it possible to use get =args= type without their name? That's how!
   #+BEGIN_SRC C++
     template <typename T>
     T& declval();

     template <typename ...T>
     auto f(T &&...args) -> decltype(g(std::forward<T>(declval<T>())...)) {…}
   #+END_SRC


   That's how things were done in ~C++11~. In ~C++14~ it is possible to omit =->
   decltype(…)=. However, removing =decltype= means, that possible substitution
   failure is no longer in the function signature but in the function body. That
   limits use of the function in ~SFINAE~.

** two forms of =decltype=
*** ~decltype(variable)~
    Returns static type of ~variable~ (how it was declared).
    #+BEGIN_SRC C++
      int a; decltype(a); // int
      int &b; decltype(b); // int &
    #+END_SRC
*** ~decltype (expression)~
    | prvalue | T   |
    | lvalue  | T&  |
    | xvalue  | T&& |
    
    #+BEGIN_SRC C++
      T f();
      T &&g();

      int main() {
          T a = f(); // f() here is ~prvalue~
          T&& b = g(); // g() here is ~xvalue~
          decltype((a)) // note the parentheses: result is T&
              decltype((b)) // result is T&
              decltype(f()) // result is T
              decltype(g()) // result is T&&
              }
    #+END_SRC
    
* =nullptr_t=
** rationale
   Type of =nullptr= is =nullptr_t=. Why? Why not =void *=?
   #+BEGIN_SRC C++
     int *p = 0; // all perfect, all valid: p is null pointer now.
     void g(void *);
     template <typename T>
     void f(T&& arg) { g(forward<T>(arg)); }
     …
     f(0); // doesn't work, because type of 0 is inferred to be int and not void *.
   #+END_SRC
   =nullptr_t= is just a type convertable to any pointer.
** possible implementation
   #+BEGIN_SRC C++
     template <typename R, typename T>
     using member_ptr = R (T::*)(A ...); // pointer to member function of =T=

     struct nullptr_t {
         template <typenme T>
         operator T*() const { return 0; }

         template <typename R, typename T>
         operator member_ptr<R, T>() const { return 0; }
     };

     static constexptr nullptr_t nullptr;
   #+END_SRC

* member pointers
  #+BEGIN_SRC C++
    struct foo {
        int bar;
    }

        int foo::*p = &foo::bar;
    foo v;
    v.*p; // dereferencing of member pointer
  #+END_SRC

* useful types
** =optional=
*** definition and usage
    Either stores a value or indicates its absence.
    | bool | empty | T data |
    #+BEGIN_SRC C++
      optional<int> a;
      a = 5;
      a = nullopt;
      if (a) { *a; }
    #+END_SRC
  
    #+BEGIN_SRC C++
      struct foo {
          foo(int, int, int);
      }
          optional<foo> a (in_place, 1, 2, 3); // create in-place calling constructor
                                               // with parameters 1, 2, 3
      a.emplace(4, 5, 6);
    #+END_SRC
  
*** why use =unique_ptr= over =optional=
    - =optional= stores the object inside of it, on stack, so if the object is
      big, it might be better to store it on heap (via =unique_ptr=).
    - =unique_ptr= is suitable for polymorphic objects
    - ~pimpl~: practice of storing pointer to implementation in structs, to
      lessen number of =#include='s. That's the case for =unique_ptr=
    - to store pointer to an object that doesn't change
** =variant=
*** definition and usage
    #+BEGIN_SRC C++
      variant<A, B, C> v; // v can represent either A, or B, or C
    #+END_SRC
    
    Typical usage of =variant= is via ~visitor~.
    #+BEGIN_SRC C++
      struct visitor {
          void operator()(A ) const;
          void operator()(B ) const;
      };
    #+END_SRC


    
