# Undefined Behaviour

#### aliasing

* situation in which different pointers point to the same place in memory
* C99 introduces special modificator -- `restrict`. It shows that this pointer is not aliased
* it is impossible to specify what should happen if `restrict` is not respected. Thus, it is UB

#### TBAA

* situation in which compiler assumes that pointers of different types point to different places in memory
* exception: signed and unsigned versions of one type might point to one place
* exception: `char` might alias any type

#### strict aliasing rule

* [situation in which compilers assumes
that dereferencing pointers to objects of different types will never refer to the same memory location](https://cellperformance.beyond3d.com/articles/2006/06/understanding-strict-aliasing.html)
* `-fno-strict-aliasing`
* in case there is need to treat one type as another, `memcpy` is the answer

#### other

* example of UB in libs: non-transitive comparator

* how to deal with UB:
    * add inspections
    * `-fsanitize=address,undefined`
    * special macros for inspections in libs

* core dump: `ulimit -c unlimited`

* reversible debuggers:
    * UndoDB
    * [rr](http://rr-project.org/)

* coverage:
    * `g++ -g --coverage ...`
    * `make ...`
    * `lcov --output-file cov.info ...`
    * `genhtml cov.info`

* coverage tools
    * linux: lcov
    * windows: OpenCppCoverage
