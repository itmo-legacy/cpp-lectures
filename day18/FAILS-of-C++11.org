* FAILS
** range-based for and const and non-const iterators compatibility
*** range-based for 
    It doesn't work for arrays! (because arrays don't have ~.begin()~, ~.end()~)
   
    Solution: overloaded functions ~begin(T)~, ~end(t)~! ~std::begin()~ and
    ~std::end()~ were born.
*** const and non-const iterators compatibility
    #+BEGIN_SRC C++
      #include <vector>
      #include <algorithm>

      std::vector<int>::const_iterator f(std::vector<int> const &);

      void g(std::vector<int> &v) {
          std::find(f(v), v.end(), 5); // incorrect, because different constness
          std::find(f(v), v.cend(), 5); // ok
      }
    #+END_SRC
*** conflict  
    There was ~std::begin~ but no ~std::cbegin~.

** move-semantics and lambdas
   You might pass closure by value: ~[a] {}~.
   
   You might pass closure by lvalue-reference: ~[&a] {}~.
   
   But can't pass by rvalue-reference!!
   
   C++14 fixes that with closure initializers: ~[a = std::move(a)] {}~.
   
** move-semantics and ~std::initializer_list~
   #+BEGIN_SRC C++
     #include <string>
     #include <vector>
     #include <utility>

     std::string s = "baz";
     std::vector<std::string> a = {"foo", "bar", std::move(s)};
   #+END_SRC
   
   ~std::initializer_list~ doesn't allow =move=, so =s= will be copied two times.
