# shared_ptr

* Smart pointer with reference counter.
Deletes an object only when all instances of shared_ptr
containing the reference are deleted.

* has deleter field for resources management

* aliasing constructor — see example with wheel in [cpp file](shared-ptr.cpp#L28)

* make_shared: `auto p = make_shared<type>(1, 2, 3, 4);`,
 constructs object directly in `shared_count` memory

* weak_ptr: only views the object and doesn't add to share_ptr count.
If `make_shared` was used to create `shared_ptr`, the object memory isn't released
until all `weak_ptr`s are also deleted. Destructor is called though.
It is a consequence of the fact, that object is constructed in `shared_count` memory.


