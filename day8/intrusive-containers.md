# Intrusive containers

* example: intrusive list with `next` and `prev` embedded into object

* element should satisfy some requirements

* `container_of` in linux kernel

|                                    | intrusive                               | non-intrusive |          |
|------------------------------------|-----------------------------------------|---------------|----------|
| size                               | less (?)                                | greater (?)   | +        |
| number of allocations              | less                                    | greater       | +        |
| require object modification        | yes                                     | no            | -        |
| manages objects lifetime           | no                                      | yes           | **-**    |
| exception safety                   | better (operations are usually nothrow) | worse         | +        |
| get iterator from value            | allows                                  | disallows     | +        |
| iteration without value addressing | slower                                  | faster        | - (weak) |


# Multi-index containers

* examples: bimap, lru_cache

* [Boost.MultiIndex](https://theboostcpplibraries.com/boost.multiindex)

# Heterogenous lookup

* Suppose, that you want to search for std::string but have a key of string_ref type.
You might convert string_ref to std::string but that would require an additional allocation.
Heterogenous lookup allows to search for std::string without converting string_ref.
