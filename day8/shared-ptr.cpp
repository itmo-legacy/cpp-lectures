#include <cstdint>
#include <array>
#include <memory>

template <typename T, typename D>
struct shared_ptr {
    T *obj;
    std::size_t * count;

    shared_ptr(T *ptr, D deleter) : obj(ptr) {
        try {
            count = new std::size_t(1);
        } catch (...) {
            deleter(ptr);
            throw;
        }
    }
};


struct wheel {};

struct car {
    std::array<wheel, 4> wheels;
};

std::shared_ptr<car> p;
wheel *raw = &p->wheels[2]; // can't check if wheel (and car) no longer exist
std::shared_ptr<wheel> smart(&p->wheels[2], p); // aliasing constructor, prolongs `p` life

struct B {};

struct D : B {};

std::shared_ptr<B> b;
//std::shared_ptr<D> d = static_cast<std::shared_ptr<D>>(b); // incorrect, use static_pointer_cast
