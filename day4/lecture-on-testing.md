## Exceptions processing

### Error types

1. Hardware errors are hard to fix. Possible options
    * just detection
    * correction codes
    * distributed systems
    * RAID

1. Compile-time errors
    * prefer compile-time errors over runtime errors
        * static typing
        * avoid type casts
        * `static_assert`:
            ```
             int a;
             float b;
             static_assert(sizeof a == sizeof b);
             memcpy(&a, &b, sizeof a);
            ```

1. Environment errors (propagation and logging)

1. Invariant violations (are often ignored; debug assertions)

### Ways to deal with errors

1. abort (for software that is cheap to restart)
1. propagate to caller
1. ignore (best served with logging)
1. log (might be combined with others)
1. save data + abort

// [fault injection](https://en.wikipedia.org/wiki/Fault_injection)

### Build configuration

1. debug (more assertions)
1. release

### Assertions

1. ease debug
1. help to find that invariant is broken
1. make some sort of documentation

### Should I abort the program

If others will depend on your program/library, you probably shouldn't


## Testing

1. static analysis
    * coverity
    * MSVC /analyse
    * clang static analiser
    * PVS-studio (baad)
1. dynamic analysis
    * sanitizer: address, undefined, etc
    * valgrind

### Comparison

* static analysis can detect localized errors everywhere
* dynamic analysis detects any error in the run code
* both are more effective, if code is written with using analysis in mind:
    ```
    void f(char const * a, size_t n) {    // unclear if n is indeed size of a
        for (size_t i = 0; i < n; ++i {      // msvc uses special annotations that allow to indicate that
            a[i];                                            // which eases work of analyzer
        }
    }
    ```

### Dynamic testing

1. per function-tests + coverage
1. [fuzzing](https://en.wikipedia.org/wiki/Fuzzing)
    * mutation fuzzing
    * coverage-guided fuzzing
        * AFL
        * LLVMFuzzer
    * symbolic execution
        * klee
        * MS sage

