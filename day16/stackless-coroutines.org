* Stackless coroutines
  http://cpp.mimuw.edu.pl/files/await-yield-c++-coroutines.pdf

  =yield X= means "return X", but don't terminate execution but rather start it
  from this very place the next time the function is called.
  
  Actually, I understand little to nothing, so don't count on this excerpt to
  much (at all). And that's too bad, because there gonna be questions about
  stackless and stackful coroutines on the exam.
  
 #+BEGIN_SRC C++
   struct generator {
       optional<int> next() {
           switch (state) {…}
       }

       ? state;
       int i;
   };
 #+END_SRC

#+BEGIN_SRC C++
  generator<int> f() {
      struct frame_type {
          generator<int>::promise_type promize;
          // local variables, parameters
      };

      co_yield 5;
  }
#+END_SRC 
