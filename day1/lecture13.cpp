#include <cstdlib>
#include <cstring>
#include <utility>

struct string {
    string() : data(strdup("")),
               size(0),
               capacity(0) {}

    string(char const *s) : data(strdup(s)),
                            size(strlen(s)),
                            capacity(size) {}

    string(string const &other) {
        //...
    }

    ~string() {
        free(data);
    }

    size_t get_size() const noexcept {
        return size;
    }

    string &operator=(string rhs) {
        swap(rhs);
        return *this;
    }

    void swap(string &other) {
        std::swap(data, other.data);
        std::swap(size, other.size);
        std::swap(capacity, other.capacity);
    }

private:
    char *data;
    size_t size;
    size_t capacity;
};

void f_basic(string &);

void f_string(string &s) {
    string copy = s;
    f_basic(copy);
    s.swap(copy);
}

template<typename T>
struct unique_ptr {
    unique_ptr() : p(nullptr) {}

    unique_ptr(T *p) : p(p) {}

    unique_ptr(unique_ptr const &) = delete;

    unique_ptr &operator=(unique_ptr const &) = delete;

    ~unique_ptr() { delete p; }

    T &operator*() const { return *p; }

    T *operator->() const { return p; }

    T *get() const { return p; }

    void reset(T *q) {
        delete p;
        p = q;
    }

    T *release() {
        T *tmp = p;
        p = nullptr;
        return tmp;
    }

private:
    int *p;
};


int main() {
}

