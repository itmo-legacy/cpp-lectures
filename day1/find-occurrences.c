#include <string.h>
#include <stdio.h>

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Only two arguments are allowed\n");
        return 0;
    }

    const char *filename = argv[1];
    const char *word = argv[2];
    const size_t WORD_SIZE = strlen(word);

    FILE *file = fopen(filename, "r");

    char buffer[WORD_SIZE];
    if (fread(buffer, sizeof(char), WORD_SIZE, file) != WORD_SIZE) {
        return 0;
    }

    if (memcmp(word, buffer, WORD_SIZE) == 0) {
        printf("0\n");
    }
    size_t count = WORD_SIZE;
    int character_read;
    while ((character_read = fgetc(file)) != EOF) {
        char temp[WORD_SIZE];
        memcpy(temp, buffer + 1, WORD_SIZE - 1);
        memcpy(buffer, temp, WORD_SIZE - 1);
        buffer[WORD_SIZE - 1] = (char) (character_read);
        if (memcmp(buffer, word, WORD_SIZE) == 0) {
            printf("%zu\n", count + 1 - WORD_SIZE);
        }
        ++count;
    }

    fclose(file);
    return 0;
}
