#include <cstddef>
#include <cstdlib>
#include <iostream>

struct dynamic_storage {
    std::size_t ref_counter;
    std::size_t capacity;
    char data[];        // lay at the end of struct: flexible array member (https://stackoverflow.com/questions/14643406/whats-the-need-of-array-with-zero-elements)
};

dynamic_storage *new_dynamic_storage(std::size_t capacity) {
    dynamic_storage *result = (dynamic_storage *) malloc(sizeof(dynamic_storage) + sizeof(char) * capacity);
    result->ref_counter = 1;
    result->capacity = capacity;
    return result;
}

struct string {
    std::size_t size;

    union {
        dynamic_storage *data;
        char inplace_storage[sizeof(void *)];
    };
};

struct mytype {
    int a, b;

    mytype(int a, int b) : a(a), b(b) {
        std::cout << "constructor (" << a << ", " << b << ")\n";
    }

    ~mytype() {
        std::cout << "destructor (" << a << ", " << b << ")\n";
    }

    static void *operator new(size_t size) {
        std::cout << "mytype::operator new(" << size << ")\n";
        return ::operator new(size);
    }

    static void operator delete(void *p) {
        std::cout << "mytype::operator delete()\n";
        ::operator delete(p);
    }
};

int main() {
    std::string *sbad = (std::string *) malloc(sizeof(std::string)); // constructor is not called -- BAD
    // -----
    void *p = malloc(sizeof(std::string));
    std::string *s = new(p) std::string("Hello");
    s->append("X");
    std::cout << s << "X" << std::endl;
    s->~basic_string();
    free(s); // or free(p);
    // ------
    mytype(1, 2);
}

// separate classes for big_integer and storage (vector)
