```
struct bar {
    std::array<int, 10> data = {};
};

void f(void *result) {
    …
    ctor_big_struct(result);
}

void g(void *g) {
    …
}

void h() {
    char tmp_buf[sizeof(big_struct)];
    g(tmp_buf);
    big_struct& tmp = (big_struct&) tmp_buf;
    f(tmp); // no copy
}
```

### RVO

    struct big_struct {
        big_struct(int, int, int);
    };

    big_struct g() {
        return big_struct(1, 2, 3);
    };

* naïve implementation:

    ```
    void g(void *result) {
        char tmp[sizeof(big_struct)];
        big_struct_ctor(tmp, 1, 2, 3);
        big_struct_ctor(result, (big_struct&) tmp);
    }
    ```

* actual implementation:

    ```
    void g(void *result) {
        big_struct_ctor(result, 1, 2, 3);
    }
    ```

* behaviour found in actual implementation is called RVO

### NRVO

Sometimes a function returns only one particular variable;

    big_struct g() {
        big_struct tmp(1, 2, 3);
        …
            return tmp;
        …
        return tmp;
    }

In this case, `tmp` might be constructed in-place.

    void g(void *result) {
        big_struct_ctor(result, 1, 2, 3);
        try {
            (big_struct *)result->f();
        } catch (...) {
            (big_struct *)result->~big_struct();
            throw;
        }

### A problem with copying

```
std::vector<file_descriptor> r; // no copy_ctor

file_descriptor s;
r.push_back(s); // how to perform that?
```

In C++03 solution was to use `boost::shared_ptr<file_descriptor>`. It is costly and verbose.

### Move semantics

Move semantics gives an ability to transfer object to another location
without worrying about what happens at the previous one.

Even though it is possible to emulate this behaviour with usual copy constructors,
it doesn't meet general expectations from copying
(it is easy to spoil objects which shouldn't be spoilt).

Thus, rvalue-references were introduced.

Rvalue-references bind only to rvalues:

```
int f();

int a = 5;       // ok
int &b = a;      // ok
int &&c = a      // error
int &d = foo()   // error
int &&e = foo()  // k
```

##### nitpicks

* ```
    void foo(T&&)

    void push_back(T&& a) {
        // a here is lvalue
        // thus, static_cast is required
        foo(static_cast<T&&>(a));
        // however, it is a very common usage, so there is a special function for that
        foo(std::move<T>(a)):
    }
  ```

* When returning a local variable from a function `move` is performed.
 Thus, there is no need for it. Moreover, it might supress NRVO.

* If moving `T` might throw, `std::vector<T>` does copying else moving.

     * `noexcept` checks if expression can't throw.

     * destructors are `noexcept` by default

     * sometimes, the propery of being `noexcept` depends on template type.
       In this case, `noexcept` might take boolean value to indicate if it throws.


#### Perfect forwarding

* references collapsing
    * '&' + '&'   -> '&'
    * '&' + '&&   -> '&
    * '&&' + '&'  -> '&'
    * '&&' + '&&' -> '&&'

```
void f(int);
void f(char);

template <typename T>
void g(T &&a) {             // accepts lvalue- and rvalue- references
    f(static_cast<T&&>(a));  // forwards type as it was, i.e correct value category, const-ness and T
    // this static_cast is  equivalent to std::forward<T>
}
```


