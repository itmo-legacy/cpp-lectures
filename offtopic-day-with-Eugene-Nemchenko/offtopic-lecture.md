* `-I ${HEADERS}` -- headers path
* `-L ${LIBS}` -- libraries path
* linking -- process of making executable out of object files

### Libraries

* public interface in the form of headers
* static library (.a) is essentially an archive of object files
* shared library (.so) -- symbols are resolved dynamically. Allows to reduce binary size by not embedding library into it

### Make

Files depend on others. When those are changed the need to recompile dependants arises.
Build automation tools help to ease the process.
CMake is a system of generation of build automation tools (?).

#### [GNU make](http://opensourceforu.com/2012/06/gnu-make-in-detail-for-beginners/) and makefiles

(see pdf)

`.PHONY` allows to ignore files with the same names as targets:

![.PHONY](PHONY.png) -- if files "all", "clean" or "main" are created, they will not prevent target from building

[pre-order prerequisites](https://www.gnu.org/software/make/manual/html_node/Prerequisite-Types.html)

#### CMake

[detailed](https://docs.google.com/presentation/d/1AaD8yUj-RT0C3xThXhpEY5uXJPiyvHzMRq6CkRnEkXQ/edit#slide=id.g124ead5ef0_2_2)



